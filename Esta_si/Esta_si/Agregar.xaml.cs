﻿using Acr.UserDialogs;
using Esta_si.Conexion;
using Esta_si.Modelos;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Esta_si
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Agregar : PopupPage
    {
        public Agregar()
        {
            InitializeComponent();
            Load();
        }

        private async void Load()
        {
            try
            {
                Bd_con bs = new Bd_con();
                var te = await Task.Run(() => bs.GetDepartametnos());
                List<string> s = new List<string>();
                foreach(var i in te)
                {
                    s.Add(i.Name);
                }
                Depart.ItemsSource = s;
            }
            catch (Exception ex)
            {

            }
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            try
            {
                Bd_con bs = new Bd_con();
                UserDialogs.Instance.ShowLoading("Validando datos...", MaskType.Black);
                var cu = curso.Text;
                var cre = creditos.Text;
                var cod = codigo.Text;
                var depa = Depart.SelectedItem.ToString();
                var ss = await Task.Run(()=> bs.GetDepartametnosPorNombre(depa));
                Cursos c = new Cursos();
                c.Title = cu;
                c.Credits = Convert.ToInt32(cre);
                c.DepartamentID = ss.DepartmentID.ToString();
                c.CurseID = Convert.ToInt32(cod);
                await MyMethod(c);
                await PopupNavigation.PopAsync();
                UserDialogs.Instance.HideLoading();
            }
            catch(Exception ex)
            {

            }
        }
        async Task<Cursos> MyMethod(Cursos c)
        {
            //UserDialogs.Instance.ShowLoading("Validando datos...", MaskType.Black);
            var usu = await aqui(c);
            return usu;
        }
        private async Task<Cursos> aqui(Cursos c)
        {
            Bd_con bs = new Bd_con();
            var usu = await Task.Run(() => bs.InsertCurso(c));
            return usu;
        }
    }
}