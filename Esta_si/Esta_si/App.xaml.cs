﻿using Esta_si.Helpers;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Esta_si
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            if (Settings.GeneralSettings != null && Settings.GeneralSettings != "")
            {
                MainPage = new MainPage();
            }
            else
            {
                var login = new Login();
                login.login += Login_login;
                MainPage = login;
            }
        }

        private void Login_login(object sender, System.EventArgs e)
        {
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
