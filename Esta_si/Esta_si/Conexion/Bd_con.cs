﻿using Esta_si.Helpers;
using Esta_si.Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

namespace Esta_si.Conexion
{
    class Bd_con
    {
        public string esto()
        {
            string connectionString =
            @"Data Source=localhost;Initial Catalog=school; User Id=sa; Password=rodrigo";
            return connectionString;
        }
        public Usuario GetUsuarios(string i, string o,string t)
        {
            string GetProductsQuery = "select * from Usuario where login='" + i + "' AND clave ='" + o + "' AND Rol='"+t+"'";

            Usuario u = new Usuario();
            try
            {
                using (SqlConnection conn = new SqlConnection(esto()))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = GetProductsQuery;
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    u.id = reader.GetInt32(0);
                                    u.clave = reader.GetString(1);
                                    u.login = reader.GetString(2);
                                    u.personID = reader.GetInt32(3);
                                }
                            }
                        }
                    }
                }
                return u;
            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }
        public List<Cursos> GetCursos(string i)
        {
            string GetProductsQuery = "SELECT Course.CourseID as CourseID, Course.Title as Title, Course.Credits as Credits,Department.Name as Name FROM CourseInstructor inner join Course on CourseInstructor.CourseID = Course.CourseID inner Join Department on Course.DepartmentID = Department.DepartmentID where CourseInstructor.PersonID = '"+i+"'";
            List<Cursos> c = new List<Cursos>();
            Cursos u = new Cursos();
            try
            {
                using (SqlConnection conn = new SqlConnection(esto()))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = GetProductsQuery;
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    /*u.Credits= reader.GetInt32(2);
                                    u.DepartamentID = reader.GetString(1);
                                    u.CurseID = reader.GetInt32(1);
                                    u.Title = reader.GetString(3);*/
                                    c.Add(new Cursos{ CurseID=reader.GetInt32(0), Credits= reader.GetInt32(2), DepartamentID= reader.GetString(3),Title= reader.GetString(1) });
                                }
                            }
                        }
                    }
                }
                return c;
            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }
        public List<Departamento> GetDepartametnos()
        {
            string GetProductsQuery = "SELECT * from Department";
            List<Departamento> c = new List<Departamento>();
            Departamento u = new Departamento();
            try
            {
                using (SqlConnection conn = new SqlConnection(esto()))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = GetProductsQuery;
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    /*u.DepartmentID = reader.GetInt32(0);
                                    u.Name = reader.GetString(1);
                                    u.Budget = reader.GetDecimal(2);
                                    u.StartDate = reader.GetDateTime(3);
                                    u.Administrator = reader.GetInt32(4);*/
                                    c.Add(new Departamento { DepartmentID = reader.GetInt32(0), Name = reader.GetString(1), Budget = reader.GetDecimal(2), StartDate = reader.GetDateTime(3), Administrator=reader.GetInt32(4) });
                                }
                            }
                        }
                    }
                }
                return c;
            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }
        public Departamento GetDepartametnosPorNombre(string s)
        {
            string GetProductsQuery = "SELECT * from Department where Name='"+s+"'";
            Departamento u = new Departamento();
            try
            {
                using (SqlConnection conn = new SqlConnection(esto()))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = GetProductsQuery;
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    u.DepartmentID = reader.GetInt32(0);
                                    u.Name = reader.GetString(1);
                                    u.Budget = reader.GetDecimal(2);
                                    u.StartDate = reader.GetDateTime(3);
                                    u.Administrator = reader.GetInt32(4);
                                }
                            }
                        }
                    }
                }
                return u;
            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }
        public Cursos GetCursoPorID(string s)
        {
            string GetProductsQuery = "SELECT * from Course where CourseID='" + s + "'";
            Cursos u = new Cursos();
            try
            {
                using (SqlConnection conn = new SqlConnection(esto()))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = GetProductsQuery;
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    u.Credits = reader.GetInt32(2);
                                    u.DepartamentID = reader.GetInt32(3).ToString();
                                    u.CurseID = reader.GetInt32(0);
                                    u.Title = reader.GetString(1);
                                }
                            }
                        }
                    }
                }
                return u;
            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }
        public  Cursos InsertCurso(Cursos c)
        {
            try 
            {
                //Abrimos la conexión a la base de datos
                using (SqlConnection conn = new SqlConnection(esto()))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        SqlCommand query = conn.CreateCommand();

                        query.CommandType = CommandType.Text;

                        query.CommandText = string.Format("INSERT INTO Course(CourseID,Title,Credits,DepartmentID) VALUES ('"+ c.CurseID + "','"+ c.Title+"','"+ c.Credits+"','"+c.DepartamentID+"')");

                        int fil = query.ExecuteNonQuery();
                        if (fil > 0)
                        {
                            AgregarUnDueño(c.CurseID);
                            return c;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }

            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }
        public Cursos UpdateCurso(Cursos c)
        {
            try
            {
                //Abrimos la conexión a la base de datos
                using (SqlConnection conn = new SqlConnection(esto()))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        SqlCommand query = conn.CreateCommand();

                        query.CommandType = CommandType.Text;

                        query.CommandText = string.Format("UPDATE Course SET Title ='"+c.Title+"' ,Credits ='"+c.Credits+"' WHERE CourseID ='"+c.CurseID+"' ");

                        int fil = query.ExecuteNonQuery();
                        if (fil > 0)
                        {
                            return c;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }

            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }
        public Cursos DeleteCurso(Cursos c)
        {
            try
            {
                //Abrimos la conexión a la base de datos
                using (SqlConnection conn = new SqlConnection(esto()))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        SqlCommand query = conn.CreateCommand();

                        query.CommandType = CommandType.Text;

                        query.CommandText = string.Format("DELETE FROM Course WHERE CourseID='"+c.CurseID+"'");

                        int fil = query.ExecuteNonQuery();
                        if (fil > 0)
                        {
                            return c;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }

            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }
        private void AgregarUnDueño(int s)
        {
            try
            {
                //Abrimos la conexión a la base de datos
                using (SqlConnection conn = new SqlConnection(esto()))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        SqlCommand query = conn.CreateCommand();

                        query.CommandType = CommandType.Text;

                        query.CommandText = string.Format("INSERT INTO CourseInstructor(CourseID,PersonID) VALUES ('"+s+"','"+ Settings.GeneralSettings+"')");

                        int fil = query.ExecuteNonQuery();
                        if (fil > 0)
                        {
                            
                        }
                    }
                }

            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
        }
        public  List<CurseGrade> getData()
        {
            string GetProductsQuery = "SELECT Course.Title ,Count(StudentID) as cant FROM CourseGrade inner join Course on CourseGrade.CourseID = Course.CourseID group by Course.Title";
            List<CurseGrade> ss = new List<CurseGrade>();
            CurseGrade u = new CurseGrade();
            try
            {
                using (SqlConnection conn = new SqlConnection(esto()))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = GetProductsQuery;
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    ss.Add(new CurseGrade { cantidad = reader.GetInt32(1), nombre = reader.GetString(0) });
                                }
                            }
                        }
                    }
                }
                return ss;
            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }
    }
}
