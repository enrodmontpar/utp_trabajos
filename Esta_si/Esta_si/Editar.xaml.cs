﻿using Acr.UserDialogs;
using Esta_si.Conexion;
using Esta_si.Modelos;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Esta_si
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Editar : PopupPage
    {
        Bd_con bs = new Bd_con();
        public Editar(string s)
        {
            InitializeComponent();
            Load(s);
        }

        private async void Load(string s)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Validando datos...", MaskType.Black);
                var e = await Task.Run(()=> bs.GetCursoPorID(s));
                Titulo.Text = "Modificar el curso: " + e.Title;
                nombre.Text = e.Title;
                id.Text = e.CurseID.ToString();
                creditos.Text = e.Credits.ToString();
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }

        private async void actualizar_Clicked(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Actualizando datos...", MaskType.Black);
                Cursos c = new Cursos();
                c.Title = nombre.Text;
                c.Credits = Convert.ToInt32(creditos.Text);
                c.CurseID = Convert.ToInt32(id.Text);
                var esr = await Task.Run(()=>bs.UpdateCurso(c));
                UserDialogs.Instance.HideLoading();
                PopupNavigation.PopAsync();
                UserDialogs.Instance.Toast("Actualizado correctamente.");
            }
            catch(Exception ex)
            {
                UserDialogs.Instance.Toast("Error no se pudo Actualizar.");
            }
        }

        private async void elimnar_Clicked(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Eliminando datos...", MaskType.Black);
                Cursos c = new Cursos();
                c.Title = nombre.Text;
                c.Credits = Convert.ToInt32(creditos.Text);
                c.CurseID = Convert.ToInt32(id.Text);
                var esr = await Task.Run(() => bs.DeleteCurso(c));
                UserDialogs.Instance.HideLoading();
                PopupNavigation.PopAsync();
                UserDialogs.Instance.Toast("Eliminado.");
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Toast("Error no se pudo eliminar.");
            }
        }
    }
}