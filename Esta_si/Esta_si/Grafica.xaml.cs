﻿using Rg.Plugins.Popup.Pages;
using SkiaSharp;
using System.Collections.Generic;
using Xamarin.Forms;
using Microcharts;
using Entry = Microcharts.Entry;
using Esta_si.Conexion;
using System;

namespace Esta_si
{
    public partial class Grafica : PopupPage
    {
        Bd_con bs = new Bd_con();
        List<Entry> entries = new List<Entry>();
        /*{
            new Entry(200)
            {
                Color=SKColor.Parse("#FF1943"),
                Label ="January",
                ValueLabel = "200"
            },
            new Entry(400)
            {
                Color = SKColor.Parse("00BFFF"),
                Label = "March",
                ValueLabel = "400"
            },
            new Entry(-100)
            {
                Color =  SKColor.Parse("#00CED1"),
                Label = "Octobar",
                ValueLabel = "-100"
            },
        };*/
        public Grafica()
        {
            InitializeComponent();
            Load();
        }

        private void Load()
        {
            try
            {
                
                chartprueba.Chart= new RadialGaugeChart() { Entries = vadento() };
            }
            catch(Exception ex)
            {
                
            }
        }
        public List<Entry> vadento()
        {
            var esta = bs.getData();
            foreach (var i in esta)
            {
                Random Aleatorios = new Random();
                int Red = Aleatorios.Next(250);
                int Blue = Aleatorios.Next(250);
                int Green = Aleatorios.Next(250);
                int alpha = 100;
                string hexa = String.Format("#{0:X6}", Aleatorios.Next(0x1000000));
                String c = hexa;
                entries.Add(new Entry(i.cantidad) { Color = SKColor.Parse(c.ToString()), Label = i.nombre, ValueLabel = i.cantidad.ToString() });
            }
            return entries;
        }
    }
}