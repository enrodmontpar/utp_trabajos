﻿using Acr.UserDialogs;
using Esta_si.Conexion;
using Esta_si.Helpers;
using Esta_si.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Esta_si
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public event EventHandler login;
        public Login()
        {
            NavigationPage.SetHasBackButton(this, false);
            InitializeComponent();
        }
        protected override bool OnBackButtonPressed()
        {
            // you can put some actions here (for example, an dialog prompt, ...)
            return true;
        }

       

        private async void ingresar_Clicked(object sender, EventArgs e)
        {
            //cargando.IsVisible = true;
            if (string.IsNullOrEmpty(correo.Text))
            {
                await DisplayAlert("Error", "No puede dejar campos vacios", "Cerrar");
                correo.Focus();
                return;
            }
            else if (string.IsNullOrEmpty(password.Text))
            {
                await DisplayAlert("Error", "No puede dejar campos vacios", "Cerrar");
                password.Focus();
                return;
            }
            else
            {
                try
                {
                    var cor = correo.Text;
                    var pass = password.Text;
                    var tipo = Tipo.SelectedItem.ToString();
                    Bd_con bs = new Bd_con();
                    //var usus = bs.GetUsuarios(usu, pass);
                    //GetUsuarioRequest gu = new GetUsuarioRequest(correo.Text, pass);
                    //var usu = bs.proxy.GetUsuario(correo.Text, pass); // y ese getusuarioasync....?... aver
                    //var est = bs.GetUsuarios(usu, pass);
                    Usuario usu = await MyMethod(cor, pass,tipo);
                    if (usu != null)
                    {
                        Settings.GeneralSettings = Convert.ToString(usu.id);
                        login(null, null);
                        UserDialogs.Instance.HideLoading();
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        await DisplayAlert("ERROR!", "Correo o contraseña incorrecta", "Cerrar");
                        password.Text = string.Empty;
                    }
                }
#pragma warning disable CS0168 // La variable 'ex' se ha declarado pero nunca se usa
                catch (Exception ex)
#pragma warning restore CS0168 // La variable 'ex' se ha declarado pero nunca se usa
                {

                }
            }
        }
        async Task<Usuario> MyMethod(string s, string p,string t)
        {
            UserDialogs.Instance.ShowLoading("Validando datos...", MaskType.Black);
            var usu = await aqui(s, p,t);
            return usu;
        }
        private async Task<Usuario> aqui(string s, string p,string t)
        {
            Bd_con bs = new Bd_con();
            var usu = await Task.Run(() => bs.GetUsuarios(s, p,t));
            return usu;
        }
    }
}