﻿using Acr.UserDialogs;
using Esta_si.Conexion;
using Esta_si.Helpers;
using Esta_si.Modelos;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Esta_si
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            Load();
        }
        public async void Load()
        {
            try
            {
                if (Settings.GeneralSettings != null && Settings.GeneralSettings != "")
                {
                    List<Cursos> c  =new List<Cursos>();
                    var usu= await MyMethod(Settings.GeneralSettings);
                    foreach(var i in usu)
                    {
                        c.Add(new Cursos { CurseID=i.CurseID, Credits=i.Credits, DepartamentID=i.DepartamentID, Title=i.Title });
                    }
                    aquivanlosdatos.ItemsSource = c;
                    UserDialogs.Instance.HideLoading();
                }
            }
            catch(Exception ex)
            {

            }
        }
        async Task<List<Cursos>> MyMethod(string s)
        {
            UserDialogs.Instance.ShowLoading("Cargando datos...", MaskType.Black);
            var usu = await aqui(s);
            return usu;
        }
        private async Task<List<Cursos>> aqui(string s)
        {
            Bd_con bs = new Bd_con();
            var usu = await Task.Run(() => bs.GetCursos(s));
            return usu;
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Cargando");
                string fullPath = "";
                Frame imageSender = (Frame)sender;
                if (imageSender.GestureRecognizers.Count > 0)
                {
                    var gesture = (TapGestureRecognizer)imageSender.GestureRecognizers[0];
                    fullPath = (string)gesture.CommandParameter.ToString();
                }
                //PopupNavigation.Instance.PushAsync(new Ayuda_modal(fullPath));
                await MyMethoda(fullPath);
            }
            catch (Exception ex)
            {
            }
        }
        private async void Graf_click(object sender, EventArgs e)
        {
            await PopupNavigation.PushAsync(new Grafica());
        }

        [Obsolete]
        private async void esta_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.PushAsync(new Agregar());
        }
        async Task MyMethoda(string s)
        {
            UserDialogs.Instance.ShowLoading("Cargando...", MaskType.Black);
            await PopupNavigation.PushAsync(new Editar(s));
            UserDialogs.Instance.HideLoading();
        }
    }
}
