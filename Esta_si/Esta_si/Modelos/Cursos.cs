﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Esta_si.Modelos
{
    public class Cursos
    {
        public int CurseID { set; get; }
        public string Title { set; get; }
        public int Credits { set; get; }
        public String DepartamentID { set; get; }
    }
}