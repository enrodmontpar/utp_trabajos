﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Esta_si.Modelos
{
    class Departamento
    {
        public int DepartmentID { get; set; }
        public string Name { set; get; }
        public decimal Budget { get; set; }
        public DateTime StartDate {set;get;}
        public int Administrator { set; get; }
    }
}
