﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Esta_si.Modelos
{
    public class Personas
    {
       public int PersonID { get; set; }
       public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime HireDate { get; set; }
        public DateTime EnrollmentDate { get; set; }
    }
}
